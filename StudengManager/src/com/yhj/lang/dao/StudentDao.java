package com.yhj.lang.dao;

import com.yhj.lang.domain.Student;

public class StudentDao {
    private static Student[] stus = new Student[5];

    public boolean addStudent(Student stu){
        //假设数组中没有null
        int index = -1;
        //遍历数组
        for (int i = 0; i < stus.length; i++) {
            if(stus[i] == null){
                index = i;
                break;
            }
        }

        if(index == -1){
            return false;
        }else{
            stus[index] = stu;
            return true;
        }
    }
    //返回数据
    public Student[] fingAllStudent() {
        return stus;
    }
    //通过将数据替换成null的方法删除数据
    public void deleteStudentById(String delId) {
        int index = getIndex(delId);

        stus[index] = null;
    }

    //找出Id所在的索引位置
    public int getIndex(String id){
        //假设
        int index = -1;
        //遍历数组
        for (int i = 0; i < stus.length; i++) {
            Student stu = stus[i];
            if(stu != null && stu.getId().equals(id)){
                index = i;
                break;
            }
        }
        return index;
    }

    public void upDateStudent(String upId, Student newstu) {
        int index = getIndex(upId);

        stus[index] = newstu;
    }
}
