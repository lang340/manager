package com.yhj.lang.dao;

import com.yhj.lang.domain.Teacher;

public class TeacherDao {

    private static Teacher[] teas = new Teacher[5];


    //判断添加的学号是否存在
    public boolean addTeacher(Teacher tea) {

        boolean exists = false;

        for (int i = 0; i < teas.length; i++) {
            if(teas[i] == null){
                teas[i] = tea;
                exists = true;
                break;
            }
        }
        return exists;
    }

    //返回数组
    public Teacher[] findAllTeacher(){
        return teas;
    }

    //查看工号所在的索引
    public int getIndex(String tid){
        int index =-1;

        for (int i = 0; i < teas.length; i++) {
            Teacher tea = teas[i];
            if (tea != null && tea.getId().equals(tid)){
                index = i;
                break;
            }
        }
        return index;
    }

    public void deleteTeacher(String tid) {
        int index = getIndex(tid);

        teas[index] = null;
    }

    public void upDateTeacher(String tid, Teacher tea) {
        int index = getIndex(tid);

        teas[index] = tea;
    }
}
