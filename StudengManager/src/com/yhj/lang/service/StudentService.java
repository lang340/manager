package com.yhj.lang.service;

import com.yhj.lang.dao.StudentDao;
import com.yhj.lang.domain.Student;

public class StudentService {
    //先创建Dao仓库对象
    private StudentDao studentDao = new StudentDao();

    public boolean addStudent(Student stu) {

        return studentDao.addStudent(stu);

    }

    //判断Id是否存在
    public boolean isExists(String id) {
        Student[] stus = studentDao.fingAllStudent();
        //假设数组已满 没有了null,表示不存在
        boolean exists = false;
        for (int i = 0; i < stus.length; i++) {
            Student stu = stus[i];
            if (stu != null && stu.getId().equals(id)) {
                exists = true;
                break;
            }
        }
        return exists;
    }
    //查看
    public Student[] findAllStudent() {
        //获取数组，定一个变量接收
        Student[] stus = studentDao.fingAllStudent();
        //定义一个boolean变量为false
        boolean flag = false;
        //遍历数组查看是否有null
        for (int i = 0; i < stus.length; i++) {
            Student stu = stus[i];
            if (stu != null) {
                flag = true;
            }
        }

        if (flag) {
            return stus;
        } else {
            return null;
        }
    }

    public void deleteStudentById(String delId) {
        //接收的数据传递到下一级
        studentDao.deleteStudentById(delId);
    }

    public void upDateStudent(String upId, Student newstu) {
        studentDao.upDateStudent(upId,newstu);
    }
}
