package com.yhj.lang.service;

import com.yhj.lang.dao.TeacherDao;
import com.yhj.lang.domain.Teacher;

public class TeacherService {

    private TeacherDao teacherDao = new TeacherDao();

    //添加数据
    public boolean addTeacher(Teacher tea) {
        TeacherDao teacherDao = new TeacherDao();
        return teacherDao.addTeacher(tea);

    }

    //判断工号是否存在
    public boolean tisExists(String tid) {
        Teacher[] tes = teacherDao.findAllTeacher();
        boolean result = false;

        for (int i = 0; i < tes.length; i++) {
            Teacher te = tes[i];
            if(te != null && te.getId().equals(tid)){
                result = true;
                break;
            }
        }
        return result;
    }

    //查看教师的判断
    public Teacher[] findAllTeacher(){
        Teacher[] teas = teacherDao.findAllTeacher();
        boolean result = false;

        for (int i = 0; i < teas.length; i++) {
            Teacher tea = teas[i];
            if(tea != null){
                result = true;
            }
        }

        if (result){
            return teas;
        }else {
            return null;
        }
    }

    //将需要删除的信息传递给Dao
    public void deleteTeacher(String tid) {
        teacherDao.deleteTeacher(tid);
    }

    public void upDateTeacher(String tid, Teacher tea) {
        teacherDao.upDateTeacher(tid,tea);
    }
}
