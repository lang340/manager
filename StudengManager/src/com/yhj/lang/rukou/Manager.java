package com.yhj.lang.rukou;

import com.yhj.lang.controller.StudentController;
import com.yhj.lang.controller.TeacherController;
import com.yhj.lang.domain.Teacher;

import java.util.Scanner;

public class Manager {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.println("--------校园信息管理系统--------");
            System.out.println("请输入选项： 1.学生管理  2.教师管理  3.退出");
            String choice = sc.next();

            switch(choice){
                case "1":
//                    System.out.println("学生管理");
                    StudentController scc = new StudentController();
                    scc.start();
                    break;
                case "2":
                    //System.out.println("教室管理");
                    TeacherController tea = new TeacherController();
                    tea.start();
                    break;
                case "3":
                    System.out.println("感谢使用！");
                    System.exit(0);
                default:
                    System.out.println("您的输入有误。请重新输入！");
            }
        }
    }
}
