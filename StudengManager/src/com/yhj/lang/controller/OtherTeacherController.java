package com.yhj.lang.controller;

import com.yhj.lang.domain.Teacher;
import com.yhj.lang.service.TeacherService;

import java.util.Scanner;

public class OtherTeacherController extends StudentControllerPerson {
    //开闭原则：开放更新的内容，关闭原本的代码内容
    //私有化，防止别的类使用
    private Scanner sc = new Scanner(System.in);

    //封装方法
    public Teacher fengZhuang(String tid) {

        System.out.println("请输入教师姓名：");
        String name = sc.next();
        System.out.println("请输入教师年龄：");
        int age = sc.nextInt();
        System.out.println("请输入教师教龄：");
        int tage = sc.nextInt();

        Teacher tea = new Teacher();
        tea.setIid(tid);
        tea.setName(name);
        tea.setAge(age);
        tea.setTage(tage);
        return tea;
    }
}