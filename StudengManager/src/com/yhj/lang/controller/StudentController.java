package com.yhj.lang.controller;

import com.yhj.lang.domain.Student;
import com.yhj.lang.service.StudentService;

import java.util.Scanner;

public class StudentController extends StudentControllerPerson {


    //开闭原则：开放更新的内容，关闭原本的代码内容
    //键盘录入学生信息
    private Scanner sc = new Scanner(System.in);


    //封装
    public  Student fengZhuangS(String sid){
        System.out.println("请输入姓名：");
        String name = sc.next();
        System.out.println("请输入年龄：");
        int age = sc.nextInt();
        System.out.println("请输入生日：");
        String birthday = sc.next();
        //将键盘录入的信息封装到学生对象中
        Student newstu = new Student(sid,name,age,birthday);

        return newstu;
    }

}
