package com.yhj.lang.controller;

import com.yhj.lang.domain.Teacher;
import com.yhj.lang.service.TeacherService;

import java.util.Scanner;

public class TeacherController {

    private TeacherService teacherService = new TeacherService();
    //私有化，防止别的类使用
    private Scanner sc = new Scanner(System.in);

    public void start() {
        tlo:while(true){
            System.out.println("--------欢迎使用<教师>管理系统--------");
            System.out.println("请输入选项： 1.添加老师  2.删除老师  3.修改老师  4.查看所有老师  5.退出");
            String choice = sc.next();

            switch(choice){
                case "1":
//                    System.out.println("添加老师");
                    addTeacher();
                    break;
                case "2":
//                    System.out.println("删除老师");
                    deleteTeacher();
                    break;
                case "3":
//                    System.out.println("修改老师");
                    upDateTeacher();
                    break;
                case "4":
//                    System.out.println("查看老师");
                    findAllTeacher();
                    break;
                case "5":
                    System.out.println("感谢使用<教师>管理系统");
                    break tlo;
                default:
                    System.out.println("您的输入有误，请重新输入：");
            }
        }
    }

    //封装方法
    public Teacher fengZhuang(String tid){

        System.out.println("请输入教师姓名：");
        String name = sc.next();
        System.out.println("请输入教师年龄：");
        int age = sc.nextInt();
        System.out.println("请输入教师教龄：");
        int tage = sc.nextInt();

        Teacher tea = new Teacher(tid,name,age,tage);

        return tea;
    }

    //查重教师工号
    public String chaChong(){
        String tid;
        while(true){
            System.out.println("请输入教师工号：");
            tid = sc.next();
            boolean exists = teacherService.tisExists(tid);
            if (!exists){
                System.out.println("您输入的工号不存在，请重新输入：");
            }else {
                break;
            }
        }
        return tid;
    }

    //修改老师
    private void upDateTeacher() {

         String tid = chaChong();

         Teacher tea = fengZhuang(tid);


        teacherService.upDateTeacher(tid,tea);

        System.out.println("修改成功");
    }

    //删除教师
    private void deleteTeacher() {

        String tid = chaChong();
        teacherService.deleteTeacher(tid);

        System.out.println("删除成功！");
    }

    //添加教师
    public void  addTeacher(){
        String tid;
        while(true){
            System.out.println("请输入教师工号：");
            tid = sc.next();
            boolean result = teacherService.tisExists(tid);
            if (result){
                System.out.println("您输入的工号已存在，请重新输入：");
            }else{
                break;
            }
        }

        Teacher tea = fengZhuang(tid);

        boolean result = teacherService.addTeacher(tea);

        if(result){
            System.out.println("添加成功");
        }else{
            System.out.println("添加失败");
        }
    }

    //查看教师
    public void findAllTeacher(){
        Teacher[] teas = teacherService.findAllTeacher();
        if (teas == null){
            System.out.println("目前没有任何信息，请添加后再尝试！");
            return;
        }
        System.out.println("工号\t姓名\t年龄\t教龄");
        for (int i = 0; i < teas.length; i++) {
            Teacher tea = teas[i];
            if (tea != null){
                System.out.println(tea.getId()+"\t"+tea.getName()+"\t"+tea.getAge()+"\t"+tea.getTage());

            }
        }
    }
}
