package com.yhj.lang.controller;

import com.yhj.lang.domain.Student;
import com.yhj.lang.service.StudentService;

import java.util.Scanner;

public class StudentControllerPerson {

    //创建StudentService对象的isExists方法
    private StudentService studentService = new StudentService();

    //键盘录入学生信息
    private Scanner sc = new Scanner(System.in);

    public void start() {
        Scanner sc = new Scanner(System.in);
        slo:
        while (true) {
            System.out.println("--------欢迎使用<学生>管理系统--------");
            System.out.println("请输入选项： 1.添加学生  2.删除学生  3.修改学生  4.查看所有学生  5.退出");
            String choice = sc.next();

            switch (choice) {
                case "1":
                    //System.out.println("添加学生");
                    addStudent();
                    break;
                case "2":
                    //System.out.println("删除学生");
                    deleteStudentById();

                    break;
                case "3":
                    //System.out.println("修改学生");
                    upDateSutent();
                    break;
                case "4":
                    //System.out.println("查看学生");
                    findAllStudent();
                    break;
                case "5":
                    System.out.println("感谢使用<学生>管理系统");
                    break slo;
                default:
                    System.out.println("您的输入有误，请重新输入！");
            }
        }
    }

    //封装
    public  Student fengZhuangS(String sid){
        return null;
    }

    //查看以存在的学号
    public String ScunZai(){
        String upId;
        while(true){
            System.out.println("请输入学生ID：");
            upId = sc.next();
            boolean exists = studentService.isExists(upId);
            //判断是否存在，不存在则需要一直输入
            if(!exists){
                System.out.println("您输入的id不存在，请重新输入：");
            }else{
                break;
            }
        }
        return upId;
    }
    //修改学生
    public void upDateSutent() {

        String upId = ScunZai();

        Student newstu = fengZhuangS(upId);

        //创建一个审核对象
        studentService.upDateStudent(upId,newstu);

        System.out.println("修改成功！");
    }

    //查看所有学生
    public void findAllStudent() {
        //用数组接收传入的数据
        Student[] stus = studentService.findAllStudent();

        if(stus == null){
            System.out.println("没有任何数据，请添加数据后再尝试！");
            return;
        }

        System.out.println("学号\t\t姓名\t年龄\t生日");
        for (int i = 0; i < stus.length; i++) {
            Student stu = stus[i];
            if(stu !=null){
                System.out.println(stu.getId() + "\t\t" +stu.getName()+"\t"+stu.getAge()+"\t"+stu.getBirthday());
            }
        }



    }

    //添加学生
    public void addStudent(){
        String sid;
        //判断学生id是否以存在，并且不清楚用户输入几次才使得输入的学号不重复
        while(true){
            System.out.println("请输入学号：");
            sid = sc.next();
            boolean flag = studentService.isExists(sid);
            if(flag){
                System.out.println("该学号以存在，请重新输入！");
            }else{
                break;
            }
        }

        Student stu = fengZhuangS(sid);
        //创建一个审核对象
        boolean result = studentService.addStudent(stu);
        if(result){
            System.out.println("添加成功");
        }else{
            System.out.println("添加失败");
        }

    }

    //删除学生
    public void deleteStudentById(){

        String delId = ScunZai();

        //创建一个StudentService删除对象的方法
        studentService.deleteStudentById(delId);
        //输出结果
        System.out.println("删除成功！");
    }
}
