package com.yhj.lang.domain;

public class Student extends Person {
    private String birthday;

    public Student() {

    }

    public Student(String id, String name, int age, String birthday) {
        super(id, name, age);
        this.birthday = birthday;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
}
