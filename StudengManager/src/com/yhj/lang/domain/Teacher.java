package com.yhj.lang.domain;

public class Teacher extends Person {

    private int tage;

    public Teacher(){

    }

    public Teacher(String id, String name, int age,int tage) {
        super(id, name, age);
        this.tage = tage;
    }

    public int getTage(){
        return tage;
    }

    public void setTage(int tage){
        this.tage = tage;
    }
}
